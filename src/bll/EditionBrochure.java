
package bll;

import dao.Passerelle;
import entites.BateauVoyageur;
import java.util.LinkedList;
import java.util.List;
import utilitaires.PDF;

public class EditionBrochure {
 public static void editerBrochure() {

        List<BateauVoyageur>  lesBateauxVoyageurs= new LinkedList<BateauVoyageur>();
        
        
        PDF brochure= new PDF ("BateauVoyageur.pdf");
        lesBateauxVoyageurs=Passerelle.chargerLesBateauxVoyageurs();
        brochure.ouvrir();
        
        for(BateauVoyageur monBatVoy: lesBateauxVoyageurs) {
            
        
        brochure.chargerImage(monBatVoy.getImageBatVot());
       
        brochure.ecrireTexte(monBatVoy.toString());

        }
        brochure.fermer();
         
     }
   
}
