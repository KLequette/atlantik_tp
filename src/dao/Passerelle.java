
package dao;

import entites.Bateau;
import entites.BateauFret;
import entites.BateauVoyageur;
import entites.Equipement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdbc.BaseServ;

public class Passerelle {

    private static Connection  cx= BaseServ.getConnexion();
    
    
    // Exécute la requête reqSQL passée en paramètre
    // et retourne le ResultSet contenant les rangées correspondantes 
   
    private static ResultSet  creerResultSet( String reqSQL){
        
        ResultSet rs=null;
        Statement requete;
        
        try {
            
             requete =  cx.createStatement();
             rs      =  requete.executeQuery(reqSQL);
        } 
        
        catch (SQLException ex) { Logger.getLogger(Passerelle.class.getName()).log(Level.SEVERE, null, ex); }
        
        return rs;
    }
    
    // Retourne la liste des Equipements du bateau dont l'identifiant est passé en paramètre
    
    private static List<Equipement>     chargerLesEquipements(Long unIdBateau) {
        
        List<Equipement>  lesEquipements= new LinkedList<Equipement>();
        
        try {
                 
            String reqSQL="SELECT E.id, E.lib FROM EQUIPEMENT E, POSSEDER P "+
                          "WHERE  P.idBat='" + unIdBateau + "' "+
                          "AND    P.idEquip = E.id ";
            
            ResultSet rs= creerResultSet(reqSQL);
           
            while ( rs.next() ){
          
                   Equipement eqpt= new Equipement
                                    (
                                     rs.getLong("id"),   // Accès à la colonne de nom id
                                     rs.getString("lib") // Accès à la colonne de nom lib
                                    );
                   
                   lesEquipements.add(eqpt);
            }           
        } 
        
        catch (SQLException ex) { Logger.getLogger(Passerelle.class.getName()).log(Level.SEVERE, null, ex);}
         
        return lesEquipements;
    }
    
    
    // Instancie et retourne la liste d'objets de la classe BateauVoyageur, à partir des données lues dans la base 
    // Cette méthode instancie également la liste lesEquipements de chaque objet de la classe BateauVoyageur.
    
    public static List<BateauVoyageur> chargerLesBateauxVoyageurs() {
    
      
        List<BateauVoyageur>  lesBateauxVoyageurs= new LinkedList<BateauVoyageur>();
        List<Equipement>  mesEquip= new LinkedList<Equipement>();
        try {
                 
            String reqSQL="SELECT B.IDBAT, B.NOM, B.LONGUEUR, B.LARGEUR, B.VITESSE, B.TYPE, B.IMAGE FROM BATEAU B "+
                          "WHERE  B.TYPE='v'";
            
            ResultSet rs= creerResultSet(reqSQL);
           
            while ( rs.next() ){
          
                   BateauVoyageur btvg= new BateauVoyageur
                                    (
                                     rs.getFloat("vitesse"), // Accès à la colonne de nom vitesse
                                     rs.getString("nom"), // Accès à la colonne de nom nom
                                     mesEquip,
                                     rs.getLong("longueur"), // Accès à la colonne de nom longueur
                                     rs.getString("image"), // Accès à la colonne de nom image
                                     rs.getLong("largeur"), // Accès à la colonne de nom largeur
                                     rs.getLong("idbat")   // Accès à la colonne de nom id
                                    );
                   
                   lesBateauxVoyageurs.add(btvg);
            }           
        } 
        
        catch (SQLException ex) { Logger.getLogger(Passerelle.class.getName()).log(Level.SEVERE, null, ex);}
         
        return lesBateauxVoyageurs;
    }
     // Instancie et retourne la liste d'objets de la classe BateauFret, à partir des données lues dans la base 
   
    
    public static List<BateauFret> chargerLesBateauxFrets(){
        
        return null;
    }
    
    
    
    // Instancie et retourne soit  objet de la classe BateauVoyageur , soit un  BateauFret à partir des données lues dans la base
    // en fonction du type v ou f lu dans la table BATEAU
    public static Bateau           chargerUnBateau(Long idBat){
    
          
        return null;
    }
    
}


