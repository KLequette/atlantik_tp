package entites;
import java.util.List;

public class BateauVoyageur extends Bateau{
    
    private Float                 vitesseBatVoy;
    private String                imageBatVot;
    private List<Equipement>      lesEquipements;



    public BateauVoyageur(Float vitesseBatVoy, String imageBatVot, List<Equipement> lesEquipements, Long idBat, String nomBat, Long longueurBat, Long largeurBat) {
        super(idBat, nomBat, longueurBat, largeurBat);
        this.vitesseBatVoy = vitesseBatVoy;
        this.imageBatVot = imageBatVot;
        this.lesEquipements = lesEquipements;
    }

    
    
    /*
      La méthode toString  doit  retourner par exemple la chaîne suivante ( avec les sauts de ligne )  
      
      Nom du bateau: Luce Isle
      Longueur: 37 mètres 
      Largeur: 9 mètres
      Vitesse: 26 noeuds
      
      Liste des Equipements:
      
             - Accès Handicapé
             - Bar
             - Pont Promenade
             - Salon Vidéo   
     */
        
    @Override
    public String toString() {
      String ch;
      
      ch=super.toString();
      ch=ch+"Vitesse : "+ this.vitesseBatVoy + "\n" +"Liste des equipements du bateau : "+"\n";
      for(Equipement unEquip: lesEquipements){ ch=ch+"-"+unEquip.toString()+"\n";
      }
      return ch;
    }

    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
   
    public Float getVitesseBatVoy() {
        return vitesseBatVoy;
    }
    
    public void setVitesseBatVoy(Float vitesseBatVoy) {
        this.vitesseBatVoy = vitesseBatVoy;
    }
    
    public String getImageBatVot() {
        return imageBatVot;
    }
    
    public void setImageBatVot(String imageBatVot) {
        this.imageBatVot = imageBatVot;
    }
    
    
    public List<Equipement> getLesEquipements() {
        return lesEquipements;
    }

    public void setLesEquipements(List<Equipement> lesEquipements) {
        this.lesEquipements = lesEquipements;
    }
        
    //</editor-fold>   
} 


